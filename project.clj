(defproject herrostats "0.1.0-SNAPSHOT"
  :description "Display Tyler Herro's stats"
  :url "https://herrostats.herokuapp.com"
  :min-lein-version "2.0.0"
  :license {:name "Eclipse Public License v1.0"
  :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [compojure "1.6.1"]
                 [hiccup "1.0.5"]
                 [clj-http "3.10.0"]
                 [org.clojure/data.json "0.2.6"]
                 [ring/ring-jetty-adapter "1.7.1"]
                 [ring/ring-defaults "0.3.2"]
                 [cheshire "5.8.0"]
                 [environ "1.1.0"]]
  :plugins [[environ/environ.lein "0.3.1"]]
  :hooks [environ.leiningen.hooks]
  :uberjar-name "herrostats-standalone.jar"
  :profiles {:production {:env {:production true}}})
