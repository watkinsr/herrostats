(ns herrostats.view
  (:use hiccup.page hiccup.element)
  (:require
    [hiccup.page :as h]
    [hiccup.page :refer [html5]]
    ))

(defn stats [stats]
  (h/html5
   [:head
    [:title "Stats"]
    ]
   [:body
    [:div {:class "card text-center"}
     [:h1 "Tyler Herro"]
     [:p "Latest stats"]
     [:br]
     [:table  {:class "table table-bordered"}
      [:th "TEAM"]
      [:th "PTS"]
      [:th "REB"]
      [:th "STL"]
      [:th "AST"]
      [:tr
       [:td "MIA"]
       [:td (:ppg stats)]
       [:td (:rpg stats)]
       [:td (:spg stats)]
       [:td (:apg stats)]
       ]
      ]]]))
