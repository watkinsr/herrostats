(ns herrostats.core
  (:require
            [herrostats.view :as view]
            [herrostats.db :as db]))

(defn stats []
  (view/stats (db/stats)))
