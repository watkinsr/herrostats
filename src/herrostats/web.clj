(ns herrostats.web
   (:require [compojure.core :refer [defroutes GET PUT POST DELETE ANY]]
             [compojure.handler :refer [site]]
             [compojure.route :as route]
             [clojure.java.io :as io]
             [herrostats.core :as core]
             [ring.adapter.jetty :as jetty]
             [environ.core :refer [env]]))

(defroutes app
  (GET "/" [] 
       (core/stats))
  (ANY "*" []
       (route/not-found "Not Found")))

(defn -main [& [port]]
    (let [port (Integer. (or port (env :port) 5000))]
         (jetty/run-jetty (site #'app) {:port port :join? false})))
