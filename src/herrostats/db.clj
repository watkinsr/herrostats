(ns herrostats.db
  (:require [clj-http.client :as client]
            [clojure.walk :refer :all]))

(def data-nba-stats-json-v1 "http://data.nba.net/data/10s/prod/v1/2019/players/1629639_profile.json")

(defn stats []
  (let [response (:body (client/get data-nba-stats-json-v1 {:as :json}))]
    (def res (get-in response [:league :standard :stats :latest]))
    res
    ))
